import json
import tweepy


def load_tweets():
    with open('../data/unique_tweets.json', 'r') as file:
        return json.load(file)


def load_relevant_tweets():
    with open('../data/relevant_tweets.json', 'r') as file:
        return json.load(file)


def load_users():
    with open('../data/users.json', 'r') as file:
        return json.load(file)


def load_relevant_users():
    with open('../data/relevant_users.json', 'r') as file:
        return json.load(file)


def load_user_countries():
    with open('../data/user_country.json') as userCountry:
        return json.load(userCountry)


def get_tweepy_api():
    consumer_key = 'pck4dCX9qqEQdMszksHPuvp2x'
    consumer_secret = 'MJot1CObgWyVRPa8DEzI9yPHxdSk3MiqPXKR5GFro0dsBU4F0R'
    access_token = '708786576-DYm03U82zfbkVTBI4CmpIkIFmD9ywwsoGATvkOrw'
    access_token_secret = 'a09Ea3lfMioJV5auXE9ZGedGqPpUQxb5hqmoJEzazjjOw'

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    return tweepy.API(auth)
