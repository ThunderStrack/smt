import json

from scripts import utils


def main():
    data = utils.load_tweets()
    relevant_words = ['cpu', 'intel', 'attack', 'amd', 'i7', 'i5', 'i3', 'cache', 'processor', 'chip',
                      'x86', 'arm', 'microsoft', 'apple', 'cybersec', 'patch', 'branch', 'rowhammer',
                      'cache', 'covert channel', 'side channel', 'branch prediction', 'branch predictor',
                      'kernel', 'virtual address space', 'rdtsc', 'hardware', 'microcode', 'asm', 'out-of-order',
                      'execution', 'cortex', 'process', 'thread', 'memory', 'L1', 'L2', 'L3', 'pti',
                      'kpti', 'kaiser', 'tlb', 'sandbox', 'gruss', 'schwarz', 'lipp', 'fogh', 'prescher',
                      'haas', 'horn', 'mangard', 'kocher', 'genkin', 'yarom', 'hamburg', 'tugraz']
    relevant_usernames = ['lavados', 'misc0110', 'mlqxyz', 'stefanmangard', 'cyberustech', 'gdata',
                          'tehjh', 'yuvalyarom']

    relevant_tweets = extract_relevant_tweets(data, relevant_words, relevant_usernames)

    # for tweet in relevantTweets:
    #    print(tweet['text'])
    print(len(relevant_tweets))

    rt_file = open('../data/relevant_tweets.json', 'w')
    json.dump(relevant_tweets, rt_file)


def extract_relevant_tweets(tweets, keywords, users):
    relevant_tweets = []
    for tweet in tweets:
        if any(kw in tweet['text'].lower() for kw in keywords) or \
                any(user in tweet['text'].lower() for user in users) or \
                any(user in tweet['username'].lower() for user in users):
            relevant_tweets.append(tweet)
    return relevant_tweets


if __name__ == '__main__':
    main()
