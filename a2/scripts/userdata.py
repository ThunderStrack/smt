import json
from time import sleep

from tweepy import RateLimitError

from scripts import utils


def main():
    users = utils.load_relevant_users()
    api = utils.get_tweepy_api()

    api_users = []

    i = 0
    while i < len(users):

        if i != 0 and i % 450 == 0:
            with open('../data/relevant_user_data_' + str(i) + '.json', 'w') as file:
                json.dump(api_users, file)

        user = users[i]
        print('Querying user', i, 'called', user['user_name'])

        try:
            api_user = api.get_user(user['user_name'])
            api_users.append(api_user._json)
        except RateLimitError:
            i -= 1
            print('Exceeded limit, going to sleep. Good night...')
            sleep(60)  # sleep for 1 minute then check again
        except Exception as ex:
            print(ex)

        i += 1

    print(len(api_users))

    with open('../data/relevant_user_data_ALL.json', 'w') as file:
        json.dump(api_users, file)


if __name__ == '__main__':
    main()
