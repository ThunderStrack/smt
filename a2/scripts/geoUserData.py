import json
from geopy.geocoders import Nominatim

user_country = {}


def identify_location(location, identifier):
    location = location.lower()
    identifier = identifier.lower()
    start = location.find(identifier)
    while start != -1:
        end = start + len(identifier)
        ret = start == 0 or (not (location[start - 1].isalpha()) or location[start - 1] == '#')
        ret = ret and (len(location) == end or not (location[end].isalpha()))
        if ret:
            return ret
        start = location.find(identifier, start + 1)
    return False


def add_user(user_id, country_id):
    user_country[user_id] = country_id


with open('../data/relevant_user_data.json') as relevant_user_data:
    with open('../data/geoIdentifier.json') as geoIdentifier:
        data = json.load(relevant_user_data)
        geoId = json.load(geoIdentifier)
        identifiers = list(geoId.keys())
        users_without_location = 0
        geo_locator = Nominatim(user_agent="SMT", timeout=30)
        for i in range(0, len(data)):
            loc = data[i]['location']
            current_user_id = data[i]['id_str']
            identified = False
            if len(loc) == 0 or '.com' in loc or 'www' in loc:
                add_user(current_user_id, 'None')
                users_without_location = users_without_location + 1
            else:
                for j in range(0, len(identifiers)):
                    if identify_location(loc, identifiers[j]):
                        identified = True
                        add_user(current_user_id, geoId[identifiers[j]])
                        break
                if not identified:
                    processedLocation = 1
                    while processedLocation > 0:
                        try:
                            rl = geo_locator.geocode(loc, language="en", addressdetails=True)
                            processedLocation = 0
                            if rl is None:
                                add_user(current_user_id, 'None')
                                users_without_location = users_without_location + 1
                            else:
                                add_user(current_user_id, rl.raw['address']['country_code'].upper())
                        except Exception:
                            if processedLocation == 2:
                                processedLocation = 0
                                add_user(current_user_id, 'None')
                                users_without_location += 1
                            elif processedLocation == 1:
                                processedLocation = 2
        print("Identification percentage: %.2f%%" % (100 - (users_without_location * 100 / len(data))))
        print('Users without location: ' + str(users_without_location))
        print(user_country)

        with open('../data/user_country.json', 'w') as outfile:
            json.dump(user_country, outfile)
