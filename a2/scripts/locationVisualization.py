import json
import plotly.graph_objects as go
import pandas as pd
import pycountry
import numpy as np

country_numbers = {}


def increment_country_numbers(country_id):
    if country_id in country_numbers:
        country_numbers[country_id] = country_numbers[country_id] + 1
    else:
        country_numbers[country_id] = 1


with open('../data/relevant_tweets.json') as relevantTweets:
    with open('../data/user_country.json') as userCountry:
        relevant_tweets = json.load(relevantTweets)
        user_country = json.load(userCountry)
        user_not_found = 0
        for tweet in relevant_tweets:
            if str(tweet['author_id']) in user_country:
                increment_country_numbers(user_country[str(tweet['author_id'])])
            else:
                print('Not in country user: ' + str(tweet['author_id']))
                user_not_found = user_not_found + 1

        c = pycountry.countries

        user_count = len(user_country.keys()) + user_not_found
        tweet_count = len(relevant_tweets)

        print("| Country | Tweets | Tweets in % | Users | Users in % |")
        print("| --- | --- | --- | --- | --- |")
        for key, value in sorted(country_numbers.items(), key=lambda kv: kv[1], reverse=True):
            if "None" == key:
                user_per_country = sum(1 for value in user_country.values() if value == "None")
                tweet_per = (value * 100 / tweet_count)
                if tweet_per < 0.01:
                    tweet_per = "<0.01%"
                else:
                    tweet_per = f"{tweet_per:.2f}%"
                user_per = (user_per_country * 100 / user_count)
                if user_per < 0.01:
                    user_per = "<0.01%"
                else:
                    user_per = f"{user_per:.2f}%"
                print(f"| Unknown | {value} | {tweet_per} | {user_per_country} | "
                      f"{user_per} |")
            for i in range(0, len(c)):
                if c.objects[i].alpha_2 == key:
                    user_per_country = sum(1 for value in user_country.values() if value == key)
                    tweet_per = (value * 100 / tweet_count)
                    if tweet_per < 0.01:
                        tweet_per = "<0.01%"
                    else:
                        tweet_per = f"{tweet_per:.2f}%"
                    user_per = (user_per_country * 100 / user_count)
                    if user_per < 0.01:
                        user_per = "<0.01%"
                    else:
                        user_per = f"{user_per:.2f}%"
                    print(f"| {c.objects[i].name} | {value} | {tweet_per} | {user_per_country} | "
                          f"{user_per} |")
        print('Not found users: ' + str(user_not_found))

        loc = []
        zs = []
        texts = []
        for i in range(0, len(c)):
            if c.objects[i].alpha_2 in country_numbers:
                loc.append(c.objects[i].alpha_3)
                zs.append(country_numbers[c.objects[i].alpha_2])
                texts.append(c.objects[i].name)

        fig = go.Figure(data=go.Choropleth(
            locations=pd.Series(np.array(loc)),
            z=pd.Series(np.array(zs)),
            text=pd.Series(np.array(texts)),
            colorscale='Greens',
            autocolorscale=False,
            reversescale=False,
            marker_line_color='darkgray',
            marker_line_width=1,
            colorbar_title='Number Tweets',
        ))

        fig.update_layout(
            title_text='#meltdown #spectre Tweets from 2016 to 2020',
            geo=dict(
                showframe=False,
                showcoastlines=False,
                projection_type='equirectangular'
            ),
            annotations=[dict(
                x=0.5,
                y=0.1,
                xref='paper',
                yref='paper',
                text='Source: Twitter Inc. <a href="https://www.twitter.com">twitter.com</a>',
                showarrow=False
            )]
        )

        #fig.show()
        print("usercount", user_count)
