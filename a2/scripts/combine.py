import GetOldTweets3 as got
import timeit
import pickle
import os
import sys
import json

def main():
    if len(sys.argv) < 3:
        print("Usage: ./pyenv/bin/python combine.py <outFile name> <dir to raw data> [more dirs to raw data]")
        sys.exit(-1)
    
    for arg in sys.argv[2:]:
        if not os.path.isdir(arg):
            print(f"[ERR]: {arg} is not a dir!")
            sys.exit(-1)
        if len(os.listdir(sys.argv[2])) == 0:
            print(f"[ERR]: {arg} is empty!")
            sys.exit(-1)
    
    uniqueTweets = {}
    for arg in sys.argv[2:]:
        for dumpFile in os.listdir(arg):
            if dumpFile == "goodboy.log":
                continue
            f = open(os.path.join(arg, dumpFile), "rb")
            dump = pickle.load(f)
            
            for tweet in dump:
                uniqueTweets[tweet.id] = tweet

    uniqueJsonTweets = [serializeTweet(x) for x in list(uniqueTweets.values())]
    
    f = open(sys.argv[1], "w+")
    json.dump(uniqueJsonTweets, f)
    f.close()

def serializeTweet(tweet):
    jsonTweet = {
        'author_id': tweet.author_id,
        'date': {
            'second': tweet.date.second,
            'minute': tweet.date.minute,
            'hour': tweet.date.hour,
            'day': tweet.date.day,
            'month': tweet.date.month,
            'year': tweet.date.year,
            'tz': str(tweet.date.tzinfo),
        },
        'favorites': tweet.favorites,
        'formatted_date': tweet.formatted_date,
        'geo': tweet.geo,
        'hashtags': tweet.hashtags.split(" "),
        'id': tweet.id,
        'mentions': tweet.mentions.split(" "),
        'permalink': tweet.permalink,
        'replies': tweet.replies,
        'retweets': tweet.retweets,
        'text': tweet.text,
        'to': tweet.to,
        'urls': tweet.urls.split(" "),
        'username': tweet.username,
    }
    return jsonTweet


if __name__ == "__main__":
    start = timeit.default_timer()
    main()
    print(f"Total time taken: {timeit.default_timer() - start:.2f} s")
