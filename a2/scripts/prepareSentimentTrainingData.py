# Dataset from https://www.kaggle.com/kazanova/sentiment140/data, http://www.t4sa.it/

import csv
import json

sentiments = {0: 'neg', 1: 'neu', 2: 'pos'}


def set1():
    print("Reading csv file...")
    with open('../data/training.1600000.processed.noemoticon.csv', mode='r', newline='', encoding='latin-1') as csv_file:
        data = list(csv.reader(csv_file))

    print("Extracting relevant data...")
    train_data = []
    for line in data:
        s = int(line[0])
        if s < 2:
            s = sentiments[0]   # Negative
        elif s == 2:
            s = sentiments[1]   # Neutral
        else:
            s = sentiments[2]   # Positive

        train_data.append(dict(text=line[5], label=s))

    print("Writing json file...")
    with open('../data/training_data.json', mode='w', encoding='utf-8') as f:
        json.dump(train_data, f, ensure_ascii=False)


def set2():
    print("Prepping data...")
    with open('../data/t4sa_tweets_raw.csv', mode='r', newline='', encoding='utf-8') as file:
        tweets = {}
        for elem in csv.reader(file):
            tweets[elem[0]] = elem[1]
    with open('../data/t4sa_tweets_sentiment.tsv', mode='r', newline='', encoding='utf-8') as file:
        train_data = []
        for line in csv.reader(file):
            elems = line[0].split('\t')
            s = [elems[1], elems[2], elems[3]]  # neg, neu, pos
            train_data.append(dict(text=tweets[elems[0]], label=sentiments[s.index(max(s))]))

    print("Writing json file...")
    with open('../data/training_data.json', mode='w', encoding='utf-8') as f:
        json.dump(train_data, f, ensure_ascii=False)


if __name__ == '__main__':
    #set1()
    set2()
