##############################################################
# Adapted from https://www.digitalocean.com/community/tutorials/how-to-perform-sentiment-analysis-in-python-3-using-the-natural-language-toolkit-nltk
# Dataset from http://www.t4sa.it/
##############################################################

import random
import re
import string
import json
import pickle
from os import path
import os

import nltk
from nltk import classify, NaiveBayesClassifier
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize

import plotly.graph_objects as go
import pandas as pd
import pycountry
import numpy as np

from scripts import utils

download_dir = "../venv/nltk_data/"
classifier_file = '../data/sentiment_classifier.pkl'
training_dataset_file = '../data/training_dataset.pkl'
training_data_raw_file = '../data/training_data.json'

plot_save_dir = '../doc/images/'

sentiments = [(2, "pos"), (1, "neu"), (0, "neg")]


def main():
    print("Downloading training data...")
    nltk.download('punkt', download_dir=download_dir, quiet=True)
    nltk.download('wordnet', download_dir=download_dir, quiet=True)
    nltk.download('averaged_perceptron_tagger', download_dir=download_dir, quiet=True)
    nltk.download('stopwords', download_dir=download_dir, quiet=True)

    target_tweets = utils.load_relevant_tweets()
    user_countries = utils.load_user_countries()
    classifier = prepare_classifier()
    #classifier = None

    #tweets_classified, tweets_pos, tweets_neg, tweets_neu = classify_all_tweets(classifier, target_tweets)
    classify_tweets_by_country(classifier, target_tweets, user_countries)


def classify_all_tweets(classifier, tweets: list):
    tweets_len = len(tweets)

    positive_tweets = []
    negative_tweets = []
    neutral_tweets = []

    print("Analysing tweets...")

    for i, tweet in enumerate(tweets):
        print("Classifying...%.2f%%" % (i / tweets_len * 100), end='\r')
        text = tweet['text']
        if text in neutral_tweets or text in negative_tweets or text in positive_tweets:
            continue
        sentiment = classify_tweet(classifier, text)
        if sentiment == "pos":
            positive_tweets.append(text)
        elif sentiment == "neg":
            negative_tweets.append(text)
        else:
            neutral_tweets.append(text)

    total_checked_tweets = len(positive_tweets) + len(neutral_tweets) + len(negative_tweets)
    sentiment_pos = len(positive_tweets) / total_checked_tweets * 100
    sentiment_neg = len(negative_tweets) / total_checked_tweets * 100
    sentiment_neu = len(neutral_tweets) / total_checked_tweets * 100

    print("Sentiment of %d tweets is %.2f%% positive, %.2f%% negative and %.2f%% neutral" %
          (total_checked_tweets, sentiment_pos, sentiment_neg, sentiment_neu))

    write_to_file(positive_tweets, "../data/pos")
    write_to_file(negative_tweets, "../data/neg")
    write_to_file(neutral_tweets, "../data/neu")

    return total_checked_tweets, sentiment_pos, sentiment_neg, sentiment_neu


def classify_tweets_by_country(classifier, tweets, user_countries):
    tweets_len = len(tweets)

    country_unknown = 'None'
    min_tweets = 20

    positive_tweets = {}
    negative_tweets = {}
    neutral_tweets = {}

    print("Analysing tweets per country...")

    for i, tweet in enumerate(tweets):
        print("Classifying...%.2f%%" % (i / tweets_len * 100), end='\r')
        text = tweet['text']

        if str(tweet['author_id']) in user_countries:
            country = user_countries[str(tweet['author_id'])]
        else:
            country = country_unknown

        if country in neutral_tweets and text in neutral_tweets[country] or \
                country in negative_tweets and text in negative_tweets[country] or \
                country in positive_tweets and text in positive_tweets[country]:
            continue

        sentiment_in_country = classify_tweet(classifier, text)
        if sentiment_in_country == "pos":
            if country in positive_tweets:
                positive_tweets[country].append(text)
            else:
                positive_tweets[country] = [text]
        elif sentiment_in_country == "neg":
            if country in negative_tweets:
                negative_tweets[country].append(text)
            else:
                negative_tweets[country] = [text]
        else:
            if country in neutral_tweets:
                neutral_tweets[country].append(text)
            else:
                neutral_tweets[country] = [text]

    total_checked_tweets = {}
    sentiments_per_country = {}
    sentiment_all_pos = 0
    sentiment_all_neg = 0
    sentiment_all_neu = 0
    sentiment_in_country = {}

    for country in positive_tweets:
        total_checked_tweets[country] = len(positive_tweets[country])
    for country in negative_tweets:
        if country in total_checked_tweets:
            total_checked_tweets[country] += len(negative_tweets[country])
        else:
            total_checked_tweets[country] = len(negative_tweets[country])
    for country in neutral_tweets:
        if country in total_checked_tweets:
            total_checked_tweets[country] += len(neutral_tweets[country])
        else:
            total_checked_tweets[country] = len(neutral_tweets[country])

    num_total_checked_tweets = 0

    for country in total_checked_tweets:
        sentiment_in_country[country] = {
            "value": 0.0,
            "count": 0
        }

        sentiments_per_country[country] = {}

        if country in positive_tweets:
            sentiments_per_country[country]['pos'] = len(positive_tweets[country]) / total_checked_tweets[country] * 100
            sentiment_in_country[country]['value'] += len(positive_tweets[country])
            sentiment_in_country[country]['count'] += len(positive_tweets[country])
            sentiment_all_pos += len(positive_tweets[country])
        else:
            sentiments_per_country[country]['pos'] = 0
        if country in negative_tweets:
            sentiments_per_country[country]['neg'] = len(negative_tweets[country]) / total_checked_tweets[country] * 100
            sentiment_in_country[country]['value'] += len(negative_tweets[country]) * -1.0
            sentiment_in_country[country]['count'] += len(negative_tweets[country])
            sentiment_all_neg += len(negative_tweets[country])
        else:
            sentiments_per_country[country]['neg'] = 0
        if country in neutral_tweets:
            sentiments_per_country[country]['neu'] = len(neutral_tweets[country]) / total_checked_tweets[country] * 100
            sentiment_in_country[country]['count'] += len(neutral_tweets[country])
            sentiment_all_neu += len(neutral_tweets[country])
        else:
            sentiments_per_country[country]['neu'] = 0

        num_total_checked_tweets += total_checked_tweets[country]

    unknown_tweets = "Tweets from unknown locations: %0.2f%%" % \
                     ((total_checked_tweets[country_unknown] if country_unknown in total_checked_tweets else 0) / num_total_checked_tweets * 100)
    overall_sentiment = f"Overall sentiment of {num_total_checked_tweets} tweets: %.2f%% positive, %.2f%% neutral, %.2f%% negative" % \
                        (sentiment_all_pos / num_total_checked_tweets * 100,
                         sentiment_all_neu / num_total_checked_tweets * 100,
                         sentiment_all_neg / num_total_checked_tweets * 100)

    print(unknown_tweets)
    print(overall_sentiment)

    with open('../doc/sentiment_overall.md', mode='w') as file:
        file.write(unknown_tweets + '\n' + overall_sentiment + '\n')

    country_values = {}
    significant_country_values = {}
    for country in sentiment_in_country:
        if sentiment_in_country[country]['count'] >= min_tweets:
            significant_country_values[country] = sentiment_in_country[country]['value'] / sentiment_in_country[country]['count']
        country_values[country] = sentiment_in_country[country]['value'] / sentiment_in_country[country]['count']

    draw_map(country_values, '#meltdown #spectre Tweet sentiment from 2016 to 2020', 'sentiment_all.svg')
    draw_map(significant_country_values, '#meltdown #spectre Tweet sentiment from 2016 to 2020 with significant amount of tweets',
             'sentiment_significant.svg')

    save_sentiments(sentiments_per_country)


def classify_tweet(classifier, tweet_text: str):
    tokens = remove_noise(word_tokenize(tweet_text))
    return classifier.classify(dict([token, True] for token in tokens))


def remove_noise(tweets_tokens, stop_words=()):
    lemmatizer = WordNetLemmatizer()
    cleaned_tokens = []

    for token, tag in pos_tag(tweets_tokens):
        # Remove URLs
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', token)
        # Remove mentions
        token = re.sub('(@[A-Za-z0-9_]+)', '', token)

        if tag.startswith('NN'):  # Noun
            pos = 'n'
        elif tag.startswith('VB'):  # Verb
            pos = 'v'
        else:
            pos = 'a'

        token = lemmatizer.lemmatize(token, pos)
        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())

    return cleaned_tokens


def get_all_words(cleaned_tokens_list: list):
    for tokens in cleaned_tokens_list:
        for token in tokens:
            yield token


def get_tweets_for_model(cleaned_tokens_list: list):
    for tokens in cleaned_tokens_list:
        yield dict([token, True] for token in tokens)


def get_all_tweet_tokens(tweets):
    for tweet in tweets:
        yield word_tokenize(tweet)


def get_all_tweets_with_sentiment(tweets: list, sentiment):
    for tweet in tweets:
        if tweet['label'] == sentiment:
            yield tweet['text']


def prepare_classifier():
    if path.exists(classifier_file):
        print("Loading classifier...")
        with open(classifier_file, mode='rb') as f:
            classifier = pickle.load(f)
    else:
        print("Loading training data...")
        if path.exists(training_dataset_file):
            with open(training_dataset_file, mode='rb') as f:
                dataset = pickle.load(f)
        else:
            print("Preparing training data...")
            with open(training_data_raw_file, mode='r') as f:
                training_dataset_raw = json.load(f)

            tokenized_tweets = {}
            cleaned_tokenized_tweets = {}
            tokens_for_model = {}
            data_sets = {}

            for val, sentiment in sentiments:
                tokenized_tweets[val] = get_all_tweet_tokens(
                    get_all_tweets_with_sentiment(training_dataset_raw, sentiment))
                cleaned_tokenized_tweets[val] = [remove_noise(tokens, stopwords.words('english')) for tokens in tokenized_tweets[val]]
                tokens_for_model[val] = get_tweets_for_model(cleaned_tokenized_tweets[val])
                data_sets[val] = [(tweet_dict, sentiment) for tweet_dict in tokens_for_model[val]]

            dataset = [x for item in data_sets for x in data_sets[item]]

            with open(training_dataset_file, mode='wb') as f:
                pickle.dump(dataset, f)

        random.shuffle(dataset)

        divider = int((len(dataset) * 0.7))
        train_data = dataset[:divider]
        test_data = dataset[divider:]

        print("Training classifier...")
        classifier = NaiveBayesClassifier.train(train_data)
        print(f"Training accuracy: {classify.accuracy(classifier, test_data)}")

        with open(classifier_file, mode='wb') as f:
            pickle.dump(classifier, f)

    return classifier


def draw_map(country_values, title="", filename=None):
    c = pycountry.countries

    loc = []
    zs = []
    texts = []
    for i in range(0, len(c)):
        if c.objects[i].alpha_2 in country_values:
            loc.append(c.objects[i].alpha_3)
            zs.append(country_values[c.objects[i].alpha_2])
            texts.append(c.objects[i].name)

    fig = go.Figure(data=go.Choropleth(
        locations=pd.Series(np.array(loc)),
        z=pd.Series(np.array(zs)),
        text=pd.Series(np.array(texts)),
        colorbar=dict(
            title="Sentiment",
            titleside="top",
            tickmode="array",
            tickvals=[1, 0, -1],
            ticktext=["Positive", "Neutral", "Negative"],
            ticks="outside"
        ),
        autocolorscale=False,
        reversescale=False,
        marker_line_color='darkgray',
        marker_line_width=1,
        zmin=-1.0,
        zmax=1.0,
        zmid=0.0
    ))

    fig.update_layout(
        title_text=title,
        geo=dict(
            showframe=False,
            showcoastlines=False,
            projection_type='equirectangular'
        ),
        annotations=[dict(
            x=0.5,
            y=0.1,
            xref='paper',
            yref='paper',
            text='Source: Twitter Inc. <a href="https://www.twitter.com">twitter.com</a>',
            showarrow=False
        )]
    )

    #fig.show()

    if filename is not None:
        if not path.exists(plot_save_dir):
            os.mkdir(plot_save_dir)
        fig.write_image(plot_save_dir + filename, width=1920, height=1080)


def save_sentiments(sentiments_per_country):
    def add_line(c, pos, neu, neg):
        return '|' + c + '|' + str("%.2f%%" % pos) + '|' + str("%.2f%%" % neu) + '|' + str("%.2f%%" % neg) + '|\n'

    content = "|Country|Positive|Neutral|Negative|\n|---|---|---|---|\n"
    for cc in sentiments_per_country:
        sentiment = sentiments_per_country[cc]
        country = pycountry.countries.get(alpha_2=cc).name if cc != 'None' else 'Unknown'
        content += add_line(country, sentiment['pos'], sentiment['neu'], sentiment['neg'])

    with open('../doc/sentiments.md', mode='w') as file:
        file.write(content)


def write_to_file(data, filename):
    with open(filename + '.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


if __name__ == '__main__':
    main()
