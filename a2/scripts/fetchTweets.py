import GetOldTweets3 as got
import timeit
import pickle
import os
import time
import logging as log
import random
import sys
import datetime as dt

DATE_FORMAT = "%d-%m-%Y %H:%M:%S"

# https://www.techcoil.com/blog/how-to-save-and-load-objects-to-and-from-file-in-python-via-facilities-from-the-pickle-module/
def main():
    releaseTheGoldenRetriever()
    
def releaseTheGoldenRetriever():
    # hashtags that are queried joined with OR
    queryHashtags = ["#meltdown", "#spectre"]
    
    # target time frame
    # queryTimeFrame = {"from": dateToUnix("01-01-2017 00:00:00"), "to": dateToUnix("01-01-2020 00:00:00")}
    totalQTframe = {"from": dateToUnix("01-01-2017 00:00:00"), "to": dateToUnix("01-01-2020 00:00:00")}

    # timespan per retrieve
    adaptQTFrame = {"inc": 1.5, "dec": 3.0, "min": (60 * 1000), "max": (24 * 60 * 60 * 1000)}
    
    # sleep time after a retrieve failed, in seconds
    adaptSAFail = {"inc": 2.0, "dec": 2.0, "min": 15 * 60, "max": 30 * 60}
    
    # output dir for the tweet dumps
    outDir = "raw"
    dumpPrefix = "dump"
    
    # debug flag
    debug = False
    setupLogger(debug)
    setupOutputDir(outDir)
    
    log.info("---------------------------------------------------------")
    log.info("starting golden retriever.")
    log.info("---------------------------------------------------------")
    # track the time
    timeTrackOS = timeit.default_timer()
    
    # reset query values
    qTFrame = adaptQTFrame["min"]
    sAFail = adaptSAFail["min"]
    qTStamp = totalQTframe["to"]
    
    foundTweets = 0
    queryFailedBefore = False
    
    while True:
        nextQTFrame = 0
        nextQTStamp = 0
        nextSAFail = 0
        tweets = []
        
        log.info("------------------------------------------------------------------")
        log.info(f"querying from {unixToDate(qTStamp - qTFrame)} to {unixToDate(qTStamp)}")
        timeTrackQS = timeit.default_timer()
        
        try:
            tweetCriteria = buildQuery(queryHashtags, qTStamp - qTFrame, qTStamp)
            tweets = query(tweetCriteria, debug)
            
            if len(tweets) > 0:
                log.info("Query success!")
                storeTweets(outDir, dumpPrefix, tweets)
                foundTweets += len(tweets)
                nextQTStamp = tweetIDToUnix(min([tweet.id for tweet in tweets]))

            if len(tweets) == 0:
                if not queryFailedBefore:
                    log.info("Query partially succeeded!")
                    raise SystemExit
                else:
                    log.info("Query success!")
                    nextQTStamp = qTStamp - qTFrame

            nextQTFrame = min(int(qTFrame * adaptQTFrame['inc']), adaptQTFrame["max"])
            nextQTFrame = min(nextQTFrame, qTStamp - totalQTframe['from'])
            
            nextSAFail = adaptSAFail["min"]
            queryFailedBefore = False
            
        except SystemExit:
            log.info("Query FAILED!")
            if queryFailedBefore:
                nextSAFail = min(int(sAFail * adaptSAFail['inc']), adaptSAFail["max"])
            else:
                nextSAFail = sAFail
            
            nextQTFrame = max(int(qTFrame / adaptQTFrame['dec']), adaptQTFrame["min"])
            nextQTFrame = min(nextQTFrame, qTStamp - totalQTframe['from'])
            queryFailedBefore = True
            
            log.info(f"golden retriever taking a nap for {dt.timedelta(seconds=sAFail)}.")
            
            time.sleep(sAFail)

        timeTrackQD = timeit.default_timer() - timeTrackQS
        log.info(f"{len(tweets)} (total {foundTweets}) tweets found - took: {dt.timedelta(seconds=timeTrackQD)}")
        log.info(f"next query TimeFrame: {unixToTime(qTFrame)} -> {unixToTime(nextQTFrame)}")
        log.info(f"next pause after fail: {sAFail} -> {nextSAFail}")
        
        if nextQTFrame == 0:
            break
            
        qTFrame = nextQTFrame if nextQTFrame > 0 else qTFrame
        qTStamp = nextQTStamp if nextQTStamp > 0 else qTStamp
        sAFail = nextSAFail if nextSAFail > 0 else sAFail
        
    log.info(f"Total time taken: {timeit.default_timer() - timeTrackOS:.2f}")
    log.info("------------------------------------------------------------------")
    log.info(f"completed {', '.join(queryHashtags)}.")
    log.info(f"retrieved {foundTweets} tweets totally.")
    log.info("Good boy !!!")
    log.info("-------------------------------------------------------------------")

def query(tweetCriteria, debug=False):
    tweets = []
    if not debug:
        tweets = got.manager.TweetManager.getTweets(tweetCriteria)
    else:
        # simulate api interaction
        time.sleep(0)
        
        if random.random() > 50:
            for x in range(tweetCriteria.maxTweets):
                tweet = got.models.Tweet()
                tweet.username = "testuser"
                tweet.text = 3 * "test text test text test text test text "
                tweets.append(tweet)
        else:
            raise SystemExit
    
    return tweets


def buildQuery(hashtags, fromTime, toTime):
    assert(len(hashtags))
    searchQuery = f"{' OR '.join(hashtags)} since_id:{unixToTweetID(fromTime)} max_id:{unixToTweetID(toTime)}"
    tweetCriteria = got.manager.TweetCriteria().setQuerySearch(searchQuery).setMaxTweets(0)
    return tweetCriteria


def tweetIDToID(tweetID):
    tempID = int(tweetID)
    return str(tempID - ((tempID >> 22) << 22))


def tweetIDToUnix(tweetID):
    return (int(tweetID) >> 22) + 1288834974657

def unixToTweetID(unix):
    return str((unix - 1288834974657) << 22)


def unixToDate(unix):
    return time.strftime(DATE_FORMAT, time.localtime(unix/1000))

def dateToUnix(date):
    return int(time.mktime(time.strptime(date, DATE_FORMAT)) * 1000)

def unixToTime(unix):
    return dt.timedelta(milliseconds=unix)

def tweetIDToDate(tweetID):
    return unixToDate(tweetIDToUnix(tweetID))
    
def dateToTweetID(date):
    return unixToTweetID(dateToUnix(date))


def storeTweets(outDir, dumpPrefix, tweets):
    fileIndex = 0
    while os.path.exists(f"{outDir}/{dumpPrefix}-{fileIndex}"):
        fileIndex += 1
    
    f = open(f"{outDir}/{dumpPrefix}-{fileIndex}", "wb+")
    pickle.dump(tweets, f)
    f.close()
    
    
def setupLogger(debug):
    # filename="goodboy.log"
    if debug:
        log.basicConfig(stream=sys.stdout, format='[%(levelname)s][%(asctime)s]: %(message)s',
                    datefmt='%d.%m.%Y %H:%M:%S', level=log.DEBUG)
    else:
        log.basicConfig(filename="goodboy.log", format='[%(levelname)s][%(asctime)s]: %(message)s',
                        datefmt='%d.%m.%Y %H:%M:%S', level=log.DEBUG)


def setupOutputDir(outDir):
    # build main dir, if it does not exist
    if not os.path.exists(outDir):
        log.info(f"creating output dir {outDir}.")
        os.mkdir(outDir)
    if not os.path.isdir(outDir):
        log.error(f"output dir {outDir} is not a dir.")
        exit(-1)


def prompt_yes_no(question):
    while True:
        answer = str(input(question + "(y/N): ").lower().strip())
        if answer == "y":
            return True
        elif answer == "n" or answer == "":
            return False
        else:
            print("Invalid Input. Try again!")


if __name__ == "__main__":
    main()
