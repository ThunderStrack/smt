import json

from scripts import utils


def main():
    tweets = utils.load_tweets()

    check_duplicates(tweets)


def check_duplicates(tweets):
    rounds = -1
    for tweet in tweets:
        if tweets.index(tweet) % 1000 == 0:
            rounds += 1
            print('Checked', rounds * 1000, 'tweets for duplicates')

        occ = 0
        for tweet2 in tweets:
            if tweet['id'] == tweet2['id']:
                occ += 1

        if occ > 1:
            print('Tweet occurred', occ, 'times')


if __name__ == '__main__':
    main()
