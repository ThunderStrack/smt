from scripts import utils
import json


def main():
    tweets = utils.load_tweets()
    relevant_tweets = utils.load_relevant_tweets()

    extract_users(tweets)
    extract_users(relevant_tweets, 'relevant_users.json')


def extract_users(tweets, fname='users.json'):
    users = list({tweet['username']: {
        'user_id': tweet['author_id'],
        'user_name': tweet['username']
    } for tweet in tweets}.values())

    print(len(users))

    with open('../data/' + fname, 'w') as file:
        json.dump(users, file)


if __name__ == '__main__':
    main()
