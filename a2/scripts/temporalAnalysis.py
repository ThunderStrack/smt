from collections import OrderedDict
from os import path, makedirs

import matplotlib.pyplot as plt
import pandas as pd
from datetime import datetime
from scripts import utils

plotDir = "../doc/images/temporal/"
scatterPlotAllTweets = "_scatterplot_all_tweets"
lineChartAllTweets = "_linechart_all_tweets"
scatterPlotTimeframe = "_scatterplot_timeframe"

titleTextAllTweets = "All queried Tweets"
allTweets = "all_data"
titleTextRelevantTweets = "Relevant Tweets"
relevantTweets = "relevant_data"

bigTimeframe = {"plus": 31, "minus": 5}
smallTimeframe = {"plus": 15, "minus": 3}
numTimeframes = 5


def main():
    relevant_tweets = utils.load_relevant_tweets()
    all_tweets = utils.load_tweets()

    if not path.exists(plotDir):
        print("Creating directory for plots...")
        makedirs(plotDir, exist_ok=True)

    relevantTemporalData = getTemporalData(relevant_tweets)
    allTemporalData = getTemporalData(all_tweets)

    plotTemporalData(relevantTemporalData, relevantTweets)
    plotTemporalData(allTemporalData, allTweets)


def getTemporalData(tweets: list):
    temporalData = {}
    for tweet in tweets:
        dateList = tweet["date"]
        day = str(f"{dateList['day']:02d}")
        month = str(f"{dateList['month']:02d}")
        year = str(tweet["date"]["year"])
        date = year + "-" + month + "-" + day

        if date not in temporalData:
            temporalData[date] = 0
        else:
            temporalData[date] += 1

    return OrderedDict(sorted(temporalData.items(), key=lambda d: datetime.strptime(d[0], "%Y-%m-%d")))


def plotTemporalData(temporalData: OrderedDict, dataSet):
    dates = [pd.to_datetime(d) for d in temporalData.keys()]
    values = [val for val in temporalData.values()]
    titleText = titleTextRelevantTweets if relevantTweets == dataSet else titleTextAllTweets

    # Scatterplot over all tweets
    fig, ax = plt.subplots()
    plt.scatter(dates, values, s=1, c="#1da1f2")
    plt.title(titleText)
    plt.xlabel("timespan")
    plt.ylabel("nr. of tweets")
    fig.autofmt_xdate()
    plt.savefig(plotDir + dataSet + scatterPlotAllTweets)

    # LineChart over all tweets
    fig, ax = plt.subplots()
    plt.plot(dates, values, c="#1da1f2")
    plt.title(titleText)
    plt.xlabel("timespan")
    plt.ylabel("nr. of tweets")
    fig.autofmt_xdate()
    plt.savefig(plotDir + dataSet + lineChartAllTweets)

    # Scatterplots + LineCharts of the present spikes defined by maxIndexes
    plotTimeframes(dates, values, dataSet, titleText)


def plotTimeframes(dates, values, dataSet, titleText):
    sortedIndexes = sorted(range(len(values)), key=lambda index: values[index])[-1:0:-1]
    maxIndexes = [sortedIndexes.pop(0)]
    for index in sortedIndexes:
        if len(maxIndexes) == 6:
            break

        if index < maxIndexes[0] or index + smallTimeframe["plus"] >= len(values):
            continue

        isNextIndex = not maxIndexes[0] <= index <= maxIndexes[0] + bigTimeframe["plus"]
        if isNextIndex:
            for entry in maxIndexes[:len(maxIndexes)]:
                if isNextIndex and entry - smallTimeframe["plus"] <= index <= entry + smallTimeframe["plus"]:
                    isNextIndex = False

            if isNextIndex:
                maxIndexes.append(index)

    timeframe = bigTimeframe
    for index in maxIndexes:
        fig, ax = plt.subplots()
        plt.plot(dates[index - timeframe["minus"]: index + timeframe["plus"]],
                 values[index - timeframe["minus"]: index + timeframe["plus"]], linewidth=1, c="#1da1f2")
        plt.scatter(dates[index - timeframe["minus"]: index + timeframe["plus"]],
                    values[index - timeframe["minus"]: index + timeframe["plus"]], s=20, c="#14171A")
        plt.title(f"Detail view {str(maxIndexes.index(index))} of {titleText}")
        plt.xlabel("days")
        plt.ylabel("nr. of tweets")
        plt.grid(b=True, axis="x")
        fig.autofmt_xdate()
        plt.savefig(plotDir + dataSet + scatterPlotTimeframe + str(maxIndexes.index(index)))

        timeframe = smallTimeframe


if __name__ == "__main__":
    main()
