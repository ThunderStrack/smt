# SMT Project Log

* Aggregated around 150000 tweets containing at least `#meltdown` or `#spectre` in the time 1.1.2017 - 1.1.2020 
  * Used Python library `getOldTweets3` as official Twitter API does not allow going back further than 30 days
* Post processed list of tweets:
  * Threw out duplicates
  * Splitted hashtags and mentions
  * Stored tweets in JSON file for further Analysis
* Created list of "relevant tweets" as `#meltdown` and `#spectre` are mentioned in other contexts than only the CPU vulnerabilities.
  * Relevant tweets are tweets that contain at least one of "words of interest" besides one of the two hashtags. Such words of interests words like `intel`, `cpu`, `attack`, etc.
* Searched for user in all tweets and relevant tweets
  * created JSON files with user names and IDs for both sets
* Get User Data 
  * Made Twitter API accounts and retrieved ~23000 authors of relevant tweets
  * User data is primarily used for location retrieval/analysis